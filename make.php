<?php
	require_once 'vendor/autoload.php';

	$configurator = new s9e\TextFormatter\Configurator;

	$configurator->loadBundle('Forum');

	$_BBCODE = [
		  '[profile]{SIMPLETEXT}[/profile]'      => '<a href="https://osu.ppy.sh/u/{SIMPLETEXT}">{SIMPLETEXT}</a>',
		  '[notice]{TEXT}[/notice]'              => '<div class="well">{TEXT}</div>',
		  '[code #ignoreTags=true]{TEXT}[/code]' => '<pre>{TEXT}</pre>',
	];

	$_BBCODE['[quote author={TEXT;optional}]{TEXT}[/quote]'] = '<xsl:if test="@author"><h2><xsl:value-of select="@author" /> wrote:</h2></xsl:if><blockquote><xsl:apply-templates /></blockquote>';
	$_BBCODE['[box title={TEXT1;optional;defaultValue=Spoiler}]{TEXT2}[/box]'] = '<div class=\'js-spoilerbox bbcode-spoilerbox\'><a class=\'js-spoilerbox__link bbcode-spoilerbox__link\' href=\'#\'><i class=\'fa fa-chevron-down js-spoilerbox__arrow bbcode-spoilerbox__arrow\'></i> {TEXT1}</a><div class=\'js-spoilerbox__body\'>{TEXT2}</div></div>';

	foreach (['code', 'quote'] as $t)
		$configurator->BBCodes->delete($t) || $configurator->tags->delete($t);

	$configurator->BBCodes->add('CENTRE', $configurator->BBCodes->get('CENTER'));

	foreach ($_BBCODE as $bbcode => $template)
		$configurator->BBCodes->addCustom($bbcode, $template);

	!file_exists('bundle/') && mkdir('bundle');

	$configurator->rendering->setEngine('PHP', 'bundle/');

	echo $configurator->saveBundle('Bundle', 'bundle/Bundle.php');
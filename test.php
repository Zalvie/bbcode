<?php

	require_once 'vendor/autoload.php';

	if(!file_exists('bundle/Bundle.php'))
		include 'make.php';

	include_once 'bundle/Bundle.php';

	$xml = Bundle::parse(file_get_contents('tests/main.txt'));

	$html = Bundle::render($xml);

	echo $html;